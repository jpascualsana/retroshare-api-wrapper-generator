from unittest import TestCase
import json, logging
import wrapper_python as wrapper
import wrapper_class_python as RsClass


ACCOUNT="814228577bc0c5da968c79272adcbfce"
PASSWORD="prueba"
TOKEN='dGVzdDp0ZXN0' #test:test

# wrapper.BASICAUTH = (ACCOUNT, PASSWORD)
wrapper.TOKEN = TOKEN

# logging.basicConfig(level=logging.DEBUG, format=wrapper.LOG_FORMAT)


class TestMultiple(TestCase):
    def test_getApiVersion(self):
        res = wrapper.JsonApiServer.version()
        print (res)
        assert True

    def test_login(self):
        res = wrapper.RsLoginHelper.isLoggedIn()
        print(res)
        # Do login
        if not res['retval']:
            res = wrapper.RsLoginHelper.attemptLogin(ACCOUNT, PASSWORD)
            print(res)
            self.assertEqual(res['retval'], 0, "CANT LOG IN")
            return
        self.assertEqual(res['retval'], True, "is not loged in")

    def test_authorizedMethod(self):
        res = wrapper.RsGxsChannels.getChannelsSummaries()
        print(res)
        self.assertEqual(res['retval'], True, "Can't get channel summaries")

class TestAsyncMethods(TestCase):
    def cb(self,res):
        print("cb", res)
    def test_asyncMeth(self):
        wrapper.RsGxsChannels.turtleSearchRequest("XRCB", 300, wrCallback=self.cb, wrTimeout=4)
        # wrapper.RsGxsChannels.turtleSearchRequest("XRCB", 300, callback=cb, )


class TestCreateChannels(TestCase):
    def test_createChannelV2(self):
        res = wrapper.RsGxsChannels.createChannelV2(name="TestwithV2",
                                                       description="no description", )
        print(res)
        self.assertEqual(res['retval'], True, "Can't create channel v2")

    def test_createChannel(self):
        channelMetadata = RsClass.RsGroupMetaData(mGroupName="TestChdddannelCreation2", mGroupFlags=4, mSignFlags=520)
        channel = RsClass.RsGxsChannelGroup(mMeta=channelMetadata, mDescription="Channel Test")
        res = wrapper.RsGxsChannels.createChannel(channel)
        print(res)
        self.assertEqual(res['retval'], True, "Can't create channel")


    def test_unsubscribeChannels(self):
        channels = wrapper.RsGxsChannels.getChannelsSummaries()
        print("Channels get:", len(channels['channels']),)
        for c in channels['channels']:
            print("Procesing", c)
            if c['mSubscribeFlags'] == 7:
                print("Unsubscribing:", c['mGroupName'], c['mGroupId'])
                res = wrapper.RsGxsChannels.subscribeToChannel(channelId=c['mGroupId'], subscribe=False)
                print(res)
            else:
                print("No subscribed")